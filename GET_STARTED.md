# Get Started

## Basic
At the moment this version is live on https://tw-cm-chat.s3.nl-ams.scw.cloud/0.2/chat.html and can be used by adding ?channel=ChannelName

so https://tw-cm-chat.s3.nl-ams.scw.cloud/0.2/chat.html?channel=ChannelName

In rare occasions it sometimes fails to load/connect to twitch succesfully, it will give an alert asking you to reload, and reloading should fix the problem.

The program is written with OBS in mind, so integrating in other programs I haven't tested.

## Changing settings

When the page is open, you can get to the settings page with "ALT + s", if you have it as browsersource in OBS, rightclick the source and pick interaction, so that you can interact with the page.

### Channel to add / show chat

This option adds an [extra] channel to get the chat from. If you only want one channel and you used ?channel=channel in url then you probably will never use it. 
When the page connects to a channel for which it doesn´t have an config file, it will copy the globalSettings as settings for the channel. 

### Download config

If you added a channel, you can here download the config file, which is a copy from globalSettings, but with the right channel ID. Another way is to edit the globalSettings example config, and only store / download the channel config if you are happy with the results.

### Storing in browser

This option stores the selected channel settings in the browser and loads them when you open the page another time. So if you happy with you configuration, save it to the browser so that it will be loaded automatically.

### OBS
For now the settings page is pretty basic and you can basically upload / store a config file or add new channel to display the chat from.
OBS does not show the dropdown menu's, but if you click on one, you can select the channel with up or down key. 

Also the download button doesn't seem to work, this is why there is a textarea where the contents of config file 
In OBS currently it is not possible to download directly, but the show in textarea gives the content of the json file. Also for uploading a config, the JSON config will show in the textarea in the bottom, which you can copy.

For uploading a config, you can paste the JSON content in the textarea and click on the parse/apply below button.

While this may not be the easiest way, it allows to set everything that is implemented and save me time so that I can focus more on new features. And if you have set the config, you can leave it alone, so the hassle should be limited.

### Storing in browser

Also, don't forget to store the info in the browser by selecting the channel and click the store to browser button, so that the settings will be loaded next time without the need to upload them again


### example config

In the folder examples there is an example config file (with the default values for now), with some added explanation. Not everything is implemented yet. But this allows to keep the same config file for a while.

A channel is mainly referenced by it's id nummer instead of the name. This is maybe a bit less readably but I guess most of you only will add one channel, so it will be clear enough. 

working with the config files also allows you to test things out in chrome browser and load later the saved config file to the obs browsersource.


### CSS styles / running locally

If you want to run it as local file, you can uncomment line 52 in the settings.js file and set the right channelname there

For now, if you want to use your own CSS style, the best option is to run it locally. I want to include that you can add CSS rules to the config file, but it is not that high on the priority list because running it locally is quite easy. 

Just download the zip file (https://tw-cm-chat.s3.nl-ams.scw.cloud/0.2/tm-cm-chat.zip) of from here on gitlab.

If you don't check the local file option and just put in the file path the ?channel=Channel should still work. Othewise in the js/settings.js file you can comment out line 52 and add the channel name there so it will connect to it in the start.

