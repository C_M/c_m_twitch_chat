/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
 
class twitch_twi{
  constructor(logger){
    this._client = null
    this.connect()
    this._logger = logger
  }

  get log(){
    return this._logger
  }

  get client(){
    return this._client
  }

  readyStateString(){
    let state = this._client.ws.readyState // maybe find tmi.js function for it
    return `${state} (${this._readyStateStates[state]})`
  }  

  connected(){ // same herè, maybe find function for ws
    return (this._client?.ws?.readyState == 1)
  }

  connect(){
    this._client = new tmi.Client({
    options: { debug: true, messagesLogLevel: "info" },
                connection: {
                    reconnect: true,
                    secure: true
                  },
                // identity: {
                //   username: 'bot-name',
                //   password: 'oauth:my-bot-token'
                // },
              //  channels: [ 'my-channel' ]
                });
    this._client.connect().catch(console.error);
    this._addListeners()
  }


  _addListeners(){
    this._client.on('connected', this._onServerConnected.bind(this))
    this._client.on('message', this._onMessage.bind(this) )
    this._client.on('cheer', this._onMessage.bind(this) )
    this._client.on('roomstate', this._onRoomstate.bind(this))
    this._client.on('join', this._onJoin.bind(this))
    this._client.on('raw_message', this._onRawMessage.bind(this))
    this._client.on('messagedeleted', this._onMessageDeleted.bind(this))
    this._client.on('ban', this._onBan.bind(this))
    this._client.on('timeout', this._onTimeout.bind(this))
  }

  _onBan(channel, msg, _, msgTags){
    console.log([channel, msg, _, msgTags])
    settings.channelsByID[msgTags['room-id']].removeMSGofUser(msgTags['target-user-id'])
  }

  _onTimeout(channel, msg, _, duration, msgTags){
    console.log(channel, msg, _, duration, msgTags)
    settings.channelsByID[msgTags['room-id']].removeMSGofUser(msgTags['target-user-id'])
  }

  _onMessageDeleted(channel, username, deletedMessage, tags){
    console.log([channel, username, deletedMessage, tags])
    settings.channelsByName[channel.slice(1)].removeMSG(tags['target-msg-id'])
    return
  }

  _onRoomstate(channel, tags){
    log.info("joined channel: "+channel)
    settings.channelsByName[channel.slice(1)].roomState = tags  // slice 1 because of the # before the name
    console.log(tags)
  }

  _onRawMessage(msg, raw)
  {
    if(raw.command == 'JOIN' || raw.command == 'PART'){
      return // not going to print alll joins an parts
    }
    console.log(raw)
  }

  _onMessage(channel, tags, text, self){
    const msg = new ChatMessage(tags, text, self)
    this.log.info("message received on channel: " + channel)
    msg.process()
  }


  _onServerConnected(server, port){
          // TODO: added to channel so get the badges list (or notice that connected etc)
      // dunno if we can use the joined or we need to read rawmessage
    this.log.info(`connected to: ${server}:${port}`)
    settings.onConnected(this)

  }

  _onJoin(channel, nick, self){
    return // we do nothing with it yet
    if(self){ // i'm the one joining
      this.log.info(`joined #${channel} as ${nick}`)
    }
    else this.log.info('someone joined') // not really care about this atm
  }

  join_channel(channel_name){
    /* when channel is left and rejoined, could reload badges and create new channel
    without that need for, but also could be seen as reset / reload */
    new Channel(channel_name) 
    this._client.join(channel_name)
    // dunno what happens if tmi is already listining to channel
    this.log.info(`joining ${channel_name}`)
  }

  leave_channel(name){
    this._client.part(channel)
    //maybe also remove from local channellist
    this.log.info(`joining ${channel}`)
  }

  disconnect(){
    this._client.disconnect()
  }

  // _onClose = () => {
  //   console.log("i'm afraid this doesnt work here")
  //  this.log.info("Socket closed method called " + this.readyStateString());
  // }
}
