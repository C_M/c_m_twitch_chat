/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// if needed rewrite this to a indexeddb, but for now I guess localstorage will be good enough
class RoomUsers{
	constructor(room_id){
		console.log(`room ${room_id}`)
		this._id = room_id
		this._users = {}
		this._usersByName = {}
		this.initUsers()
			
	}

	initUsers(){
		const data = (JSON.parse(localStorage.getItem(`${this._id}_users`) )|| {})
		//console.log("init users")
		//console.log(data)
		for ( const [key, v] of Object.entries(data) ){
			// part so make sure that if channel is saved as room-id, it chat changed to channel id
			let ch = this._id
			if(v._room_id){
				ch = v._room_id
			}
			if(v._channel_id){
				ch = v._channel_id
			}
			// TODO: maybe use a dict as arg for new user, seems easier
			this._users[key] = new User(key, v._user_name, ch, v._color, v._group, v._badges_raw, v._userFont, v._msgFont, v._textColor, v._fontSize, v._msgStart, v._bonus, v._bonus_items )
			this._usersByName[v._user_name] = this._users[key]
		
		}
	}

	_dataUpdated(){
		//console.log('need to store user')
		localStorage.setItem(`${this._id}_users`, JSON.stringify(this._users) )
	}

	getUser(id){
		return this._users[id]
	}

	checkUser(user, tags){
		// for now, check everytime, if to big a strain, maybe do once several minutes
		let updated = false
		// update if user has changed color -> wont work if user set custom color with command....
		if (user._color != tags.color && tags.color != ""){
			user._color = tags.color
			updated = true
		}
		// broadcast shoudn't be able to change so leave for now
		if(user._group == 'moderator' && tags.mod != 1){
		   user._color = 'standard';
		   updated = true
		}
		if(user._badges_raw !== tags['badges-raw']){
			user._badges_raw = tags['badges-raw']
			updated = true
		}
		if(updated){
			this._dataUpdated()
		}

		return user;
	}

	// if user already exists, check if settings are the same and return user
	// otherwise create the user and return it
	getUserFromMsg(tags){
		let user = this.getUser(tags['user-id']) 
		if(user){
			return this.checkUser(user, tags)
		}
		else{
			let group = 'standard'
			if(tags['subscriber'] === 1){ group = 'subscriber'}
			if(tags['user-type'] === 'vip') { group = 'vip '}
			if(tags['mod'] == 1 ){ group = 'moderator' }
			if(tags['room-id'] === tags['user-id']){ group = 'broadcaster'}
			let color = null
			if(tags['color'] != "") {
				color = tags.color
			} 
			let nUser = User.newUser(tags['user-id'], tags['display-name'], tags['room-id'], color, group, tags['badges-raw'])
			this._users[tags['user-id']] = nUser
			this._dataUpdated()
			this._usersByName[nUser.name] = nUser
			return nUser
		}
	}
}

class User{
	constructor(user_id, userName, room_id, color, group, badges, userFont, msgFont, textColor, fontSize, msgStart, bonus, bonus_items){
		this._userID = user_id
		this._user_name = userName
		this._channel_id = room_id
		this._color = color
		this._group = group
		this._badges_raw = badges
		this._userFont = userFont
		this._msgFont = msgFont
		this._textColor = textColor
		this._fontSize = fontSize
		this._msgStart = msgStart
		this._bonus = bonus
		this._bonus_items = bonus_items
		this.messages = {}
	}
	//https://stackoverflow.com/questions/4910567/hide-certain-values-in-output-from-json-stringify
	// if messages is includes in json, we get a loop, so an error
	toJSON = function () {
	    let  result = {};
	    for (let x in this) {
	        if (x !== "messages") {
	            result[x] = this[x];
	        }
	    }
	    return result;
	}

	// returns the saved color, or calculates it from the get_color for user function, this way 
	// colors will be as intended if default colors are changed
	get color(){
		return (this._color ? this._color : settings.get_color_for_user(this._user_name))
	}

	get id(){
		return this._userID
	}

	get name(){
		return this._user_name
	}

	get group(){
		return this._group
	}

	static newUser(user_id, user_name, room_id, color, group, badges){
		return new User(user_id, user_name, room_id, color, group, badges, null, null, null, null, null, {})
	}

// this function is maybe redundant because of the tmi parsing
	get badges(){
		if( this._badges_raw == null){ // no badges so no need to try to find them
			return [];
		}
		let _badges = this._badges_raw.split(",") // get all badges in an array
		let result = []
		let ch = settings.channelsByID[this._channel_id].getBadgeSets()
		let gl = badges.getGlobalSets()
		for (var i = 0; i < _badges.length; ++i){
		  let b = _badges[i].split("/") // split badge name and version part of description
		  if(b[0] in ch && b[1] in ch[b[0]].versions){ // check if badge is defined in channel badge list
			result.push(ch[b[0]].versions[b[1]])
		  }
		  else if(b[0] in gl){ // otherwise get info from global badge list
			result.push(gl[b[0]].versions[b[1]])
		  }
		  //result += `<img src="${curr_badge.versions[b[1]].image_url_1x}">`
		}
		return result;
	}

	badgesHTML(size_url = 'image_url_1x'){
		let b = this.badges
		let result = ""
		for(let i = 0; i < b.length; ++i){
			result += `<img src="${b[i][size_url]}" class="badge badge_${b[i].title}">`
		}
		return result
	}
}