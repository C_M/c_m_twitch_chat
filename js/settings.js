/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// simple function to have a log window possible
function log_message(msg){
  let date = new Date()
  let s = `${String(date.getHours()).padStart(2, "0")}:${String(date.getMinutes()).padStart(2, "0")}:${String(date.getSeconds()).padStart(2, "0")} ${msg}`
  $('#console').append(`<p>${s}</p>`)

  console.log(s)
  $("#console").children().slice(0, -25).remove();

 // $('#chatwindow').append(`<p>${msg}</p>`)
}

var log = {}
log.level = 4
log.info = function(msg){
    log_message(msg)
}
log.debug = function(msg){
    log_message(msg)
}

var settings = {
    version: 1,
    permissionMask: {
        "bold":1,
        "italic":2,
        "underline":4,
        "strikethrough":8,
        "monospace": 16,
        "userColor": 32,
        "msgColor": 64,
        "fontSize": 128,
        "msgStart": 256
    },
    isAllowed: function(action, mask){
        return (this.permissionMask[action] & mask)
    },
	channelsByName: {},
	channelsByID: {},
	default_colors: [
            ["Red", "#FF0000"],
            ["Blue", "#0000FF"],
            ["Green", "#00FF00"],
            ["FireBrick", "#B22222"],
            ["Coral", "#FF7F50"],
            ["YellowGreen", "#9ACD32"],
            ["OrangeRed", "#FF4500"],
            ["SeaGreen", "#2E8B57"],
            ["GoldenRod", "#DAA520"],
            ["Chocolate", "#D2691E"],
            ["CadetBlue", "#5F9EA0"],
            ["DodgerBlue", "#1E90FF"],
            ["HotPink", "#FF69B4"],
            ["BlueViolet", "#8A2BE2"],
            ["SpringGreen", "#00FF7F"]
            ],
    onConnected: function(twi){
        console.log(twi)
    	let channel = window.location.href.split("channel=")
    	if(channel.length > 1){
    	  channel = channel[1].split("&")[0]; // the split is to make sure only channel var is captured
    	  channel = channel.split(',') // option to add more channels
    	  for(let i = 0; i < channel.length; ++i){
    	    twi.join_channel(channel[i])
    	  }
    	}
        //twi.join_channel("CHANNELNAME")
    },
    // TODO: this must be placed somewhere else sometime for cleaner file
    globalSettings: {
    	messageStart: ": ",
        hideMessagesStartingWith: ["!"],
        emoteThemeColor : "dark", 
        message_color: false,
        messageClasses: 'animateFromBottom color-light-grey font-larger',
        showBadgesWhenMentioned : false, 
        showUserColorWhenMentioned : true,
        //set to false to remove whole message,so 'text' or false
        //removed_message_message: 'message deleted', 
        removed_message_message: false, 
        removed_message_keep_name: false,
        removeAfter: false,
    	permission:{
            "standard":{
                "include":[],
                "mask": null,
                options:{
                    "bold":true,
                    "italic":true,
                    "underline":true,
                    "strikethrough":true,
                    "monospace": true,
                    "userColor": false,
                    "msgColor": false,
                    "fontSize": false,
                    "msgStart": false
                }
            },
            "subscriber":{
                "include":['standard'],
                "mask": null,
                options:{}

            },
            "vip":{
                "include":['subscriber'],
                "mask": null,
                options:{
                }
                
            },
            "moderator":{
                "include":['vip'],
                "mask": null,
                options:{
                    "userColor": true,
                    "msgColor": true,
                    "fontSize": true,
                    "msgStart": true
                }
                
            },
            "broadcaster":{
                "include":['moderator'],
                "mask": null,
                options:{}
            }            
        },
        css: ""

    },
    byChannel: {}

}

// function to add get the byChannel key added when 
// it is not the global settings (maybe move globalsettings)
settings.getSettingsBaseLoc = function(ch){
    return (ch == 'globalSettings'? this : this.byChannel)
}

// function to add an item to key in localstorage
settings.localAddToList = function(key, item){
    console.log(key + " " + item)
    let l = (JSON.parse(localStorage.getItem(key)) || [])
    if (!l.includes(item)) l.push(item)
    localStorage.setItem(key, JSON.stringify(l));
}

// function to remove an item to key in localstorage
settings.localRemoveFromList = function(key, item){
    let l = (JSON.parse(localStorage.getItem(key)) || [])
    for(var i = 0; i < l.length; ++i){
        if(l[i] == item) l.splice(i, 1)
    }
    localStorage.setItem(key, JSON.stringify(l));
}

// function to delete channel settings from local storage
settings.localDelChannelSettings = function(ch){
    this.localRemoveFromList('savedChannels', ch)
    localStorage.removeItem(ch)
}


// function to save channel settings from local storage
settings.saveChannelSettings = function(ch){
    localStorage.setItem(ch, JSON.stringify(this.getSettingsBaseLoc(ch)[ch]));
    this.localAddToList('savedChannels', ch) 
}

// function to save channels to join on startup *not yet used*
settings.saveChannelsToJoin = function(ch){
    localStorage.setItem(ch, JSON.stringify(this.getSettingsBaseLoc(ch)[ch]));
    this.localAddToList('joinChannels', ch) 
}

// function to save all know channel settings to local storage
settings.saveAllChannelSettings = function(){
    this.saveChannelSettings('globalSettings')
    let ch = Object.keys(settings.byChannel)
    for(var i = 0; i < ch.length; ++i){
        this.saveChannelSettings(ch[i])
    }
}


// just always update masks if called, saves an extra function for only updating
// only has to be run with adding room or updating settings, so 
// a bit of performance lost and extra work is not end of the world
function updatePermissionMask(channel, group_name){
    let mask = 0;
    let curr_perm = settings.getSettingsBaseLoc(channel)[channel]['permission']
    let curr_group = curr_perm[group_name]
    // add the extra permissions for the group to the mask
    for(const [key,bool] of Object.entries(curr_group['options'])){
        if(bool){
            mask += settings.permissionMask[key]
        }
    }
    // bitwise or the result with the herited permissions
    for(var i = 0; i < curr_group['include'].length; ++i){
        let other = updatePermissionMask(channel, curr_group['include'][i])
        curr_perm[curr_group['include'][i]]['mask'] = other
        mask |= other
    }
    curr_group.mask = mask;
    return mask

}

// update the mask for each permission lvl in a channel
function updatePermissionMaskAllGroups(channel){
    if(!isNaN(channel)) channel = parseInt(channel)
    let loc = settings.getSettingsBaseLoc(channel)
    for(const [key,bool] of Object.entries(loc[channel]['permission'])){
        updatePermissionMask(channel, key)
    }
}
// set all the masks for global, and added channels
function updateAllPermissionMasks(){
    updatePermissionMaskAllGroups('globalSettings');
    let ch = Object.keys(settings.byChannel)
    for(var i = 0; i < ch.length; ++i){
        updatePermissionMaskAllGroups(ch[i])
    }
}

//returns name if it is known, otherwise it will return ID
settings.getChannelName = function(ID){
    return (this.channelsByID[ID] ? this.channelsByID[ID].name : ID)
}

// function to load channel settings from local storage
settings.loadChannelSettings = function(ch){
    log.info(`load config from local storige for: ${ch} (${this.getChannelName(ch)})` )
  this.getSettingsBaseLoc(ch)[ch] = JSON.parse(localStorage.getItem(ch));
}

// function to load settings from all channels from  local storage
settings.loadAllChannelSettings = function(){
    let chList = (JSON.parse(localStorage.getItem('savedChannels')) || [])
    for(var i = 0; i < chList.length; ++i){
        this.loadChannelSettings(chList[i])
    }
}

// function to create json file from js object/dict and download it
settings.export = function(name, data){
    data = JSON.stringify(data, null, 2)
    let uri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(data)
    let element = document.createElement('a')
    element.setAttribute('href', uri)
    element.setAttribute('download', name)
    element.click()
}


// getSjon data settings
settings.getSettingsJSON = function(ch){
    let data = {c_m_twitch_chat_settings: {type: 'channel_settings', version: settings.version, 'channel': ch, settings: this.getSettingsBaseLoc(ch)[ch]}}
    return data
}

// function to create json file with settings and download it
settings.exportChannel = function(ch){
    this.export(`settings_${ch}.json`, this.getSettingsJSON(ch))
}

// function to upload config file and aply it
settings.applyJSONsettings = function(data){
    //maybe later relay different settings so other functions, for now we only have this
    data = data.c_m_twitch_chat_settings
    if(data === undefined){ return} // something that should be wrong is rong
        console.log("went past undef")
    if(data.type == "channel_settings"){
        let loc = this.getSettingsBaseLoc(data.channel)

        // TODO maybe error /handling if version is too low, not yet a problem but eh still
        console.log(data.settings)
        loc[data.channel] = data.settings;
        // config can be changed in json, so recalc the mask
        updatePermissionMaskAllGroups(data.channel)
        log.info(`config load from JSON for channel: ${data.channel} ${this.getChannelName(data.channel)})`)
        // TODO message that is loaded
    }

}

 // funtion to get color for a user when it is not send with message
settings.get_color_for_user = function(name) {
    var n = name.charCodeAt(0) + name.charCodeAt(name.length - 1);
    color = this.default_colors[n % this.default_colors.length][1]                
    return color;
}


settings.loadFile = function(file){
    let reader = new FileReader()
    reader.onload = function(e){
        try{
            let content = e.target.result
            let result = JSON.parse(content)
            if (result.c_m_twitch_chat_settings !== undefined){
                settings.applyJSONsettings(result)
            }

        }
        catch (err) {
            alert('error while reading file ' + err);
        }
    }
    reader.readAsText(file)
}

function show_error(msg){
    $('#errorDiv div').append(`<p class="errorMSG">${msg}</p>`)
    $('#errorDiv').css({'visibility': 'visible',  'z-index': 100})


}

function clearHideError(){
    $('#errorDiv div')[0].innerHTML = ""
    $('#errorDiv').css({'visibility': 'hidden', 'z-index': -100})
    
}


$(document).ready(function(){
    // load the settings if they are already in local storage
    settings.loadAllChannelSettings()
    // calc als the permissionmasks // for configs aren't stored locally
    updateAllPermissionMasks()
    document.getElementById("configFilesInputButton").addEventListener("click", function() {

        let files_arr = document.getElementById("configFilesInput").files
        console.log(files_arr)
        if(files_arr.length == 0){
            show_error("No files selected")
        }
        for( var i=0; i < files_arr.length; ++i){
            console.log(files_arr[i])
            settings.loadFile(files_arr[i])
        }
    });
})

