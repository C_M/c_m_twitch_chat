/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


var settingswindow = {}
// function to update the dropdown menu to select a channel to download config as json
settingswindow.updateChannelDownloadList = function(event){
	let opt = $('#settings_download_selectchannel option:not([disabled])')
	let curr_list = []
	// remove option if it is not in settings and create list for later comparison
	for(var i = 0; i < opt.length; ++i){
		if(opt[i].value == 'globalSettings') continue
		if(! settings.byChannel[opt[i].value]){
			//console.log(`remove ${opt[i].value}`)
			opt[i].remove()
		}
		
		curr_list.push( opt[i].value) 
	}
	//add option if is in settings but not in list
	let keys = Object.keys(settings.byChannel)
	for(var i = 0; i < keys.length; ++i){
		
		if(!curr_list.includes(keys[i])){
			//console.log(`add ${keys[i]}`)
			let loc = settings.getSettingsBaseLoc(keys[i])
			$('#settings_download_selectchannel').append($('<option>', {
			    value: keys[i],
			    text: `${keys[i]}`
			}));
		}
	}
}

// function to update the dropdown menu to select a channel remove config from local browser storage
// seems quite similair maybe combine with updateChannelDownloadList
settingswindow.updateChannelRemoveList = function(event){
	console.log("updating remeve list")
	let opt = $('#settings_delete_selectchannel option:not([disabled])')
	let curr_list = []
	let ch = (JSON.parse(localStorage.getItem('savedChannels')) || [])
	for(var i = 0; i < opt.length; ++i){
		if(!ch.includes(opt[i].value)){
			opt[i].remove()
		}
		else{
			curr_list.push(opt[i].value)
		}
	}

	for(var i = 0; i < ch.length; ++i){
		if(!curr_list.includes(ch[i])){
			$('#settings_delete_selectchannel').append($('<option>', {
			    value: ch[i],
			    text: `${ch[i]}`
			}));
		}
	}
}

//check if a select tag has a selected value, otherwise throw a js alert
// returns true if value is selected / otherwise returns false
settingswindow.checkChannelSelected = function(el, msg){
	if(!$(el).val() ){
		show_error(`Please select channel ${msg}`)
		return false
	}
	return true
}

// functions below are for buttonclicks, check if channel is selected and run the 
// function corresponding with the button
settingswindow.downLoadButtonClick = function(event){
	if(settingswindow.checkChannelSelected('#settings_download_selectchannel',' to export' )){
		settings.exportChannel($('#settings_download_selectchannel').val())
	}
}

settingswindow.storeBrowserButtonClick = function(event){
	if(settingswindow.checkChannelSelected('#settings_download_selectchannel',' to store in browser' )){
		settings.saveChannelSettings($('#settings_download_selectchannel').val())
	}
}


settingswindow.settingsShowButtonClick = function(event){
	if(settingswindow.checkChannelSelected('#settings_download_selectchannel',' to store in browser' )){
		let data = settings.getSettingsJSON($('#settings_download_selectchannel').val())
		$('#configFilesTextarea').val(JSON.stringify(data, null, 2))
	}
}

settingswindow.configAreaParseButtonClick = function(data){

	try{

		let data = JSON.parse($('#configFilesTextarea').val())
		settings.applyJSONsettings(data)
		log_message("Settings loaded")
	}
	catch(err){
		show_error("Something went wrong parsing the json " + err.message)

		}
}

settingswindow.delBrowserButtonClick = function(event){
	if(!settingswindow.checkChannelSelected('#settings_delete_selectchannel', 'to delete')){
		return
	}
	settings.localDelChannelSettings($('#settings_delete_selectchannel').val())
}

// function to add a channel to show the chat for
settingswindow.addChannel = function(){
	console.log("adding a channel")
	let ch = $('#add_channel_input_field')[0].value;
	if(ch == ""){
		console.log("Channel name is empty, try again")
	    show_error('Channel name is empty, try again')
		return
	}
	if(!connection.connected()){
		show_error("Not connected to twitch api at the moment, reload the page")
		return
	
	}
	console.log($('#add_channel_input_field')[0].value)
	connection.join_channel(ch)
}


// add some event listeners to buttons and the alt + s for settings / alt + b to show some loggin
$(document).ready(function(){

	$('#add_channel_input_field').keypress(function(event) {
	  if (event.keyCode == '13') {
	    settingswindow.addChannel()
	  }
	}); 
	$('#add_channel_button').click(function(event){
		settingswindow.addChannel()
	});

	$('#settings_download_selectchannel').focus(settingswindow.updateChannelDownloadList);
	$('#settingsDownloadButton').click(settingswindow.downLoadButtonClick);
	$('#settingsStoreBrowserButton').click(settingswindow.storeBrowserButtonClick);
    $('#settingsShowButton').click(settingswindow.settingsShowButtonClick);

    $('#configAreaParseButton').click(settingswindow.configAreaParseButtonClick);

	
	
	$('#settings_delete_selectchannel').focus(settingswindow.updateChannelRemoveList);
	
	$('#settings_delete_selectchannel_button').click(settingswindow.delBrowserButtonClick);



});


// auto connect list
// css styles
// removing channel to listen to