# README

## About twitch-cm-chat

This piece of software was written with the hope to create a bit more different chat overlays / windows in twitch stream. Mostly there are those that are visually quite the same as twitch chat and some are with a different style. But I didn't see something what expanded the functionality.



One of the idea's, and the one that is already is implemented is the "whatsapp" style of text formatting, so asteriks for bold text, tilde for strikethrough ect. 



At the moment the software is not really configurable yet (without changing the source), but if you compare it with the rest, I think it is usable. 



## Instructions

At the moment this version is live on https://tw-cm-chat.s3.nl-ams.scw.cloud/0.2/chat.html and can be used by adding ?channel=ChannelName

so https://tw-cm-chat.s3.nl-ams.scw.cloud/0.2/chat.html?channel=ChannelName

In rare occasions it sometimes fails to load/connect to twitch succesfully, it will give an alert asking you to reload, and reloading should fix the problem.

For more instructions to modify settings / run locally read the GET_STARTED.md



### Future features (or at least ideas)

- color the @replyed-user to the color of that user.

- possible to set different fonts / font sizes per user

- also have possibility to have username italic or bold or underlined

- Let user choose from all hex colors (and maybe rainbow colors)



## Using it?

If you tried it and like the software, we would love to hear it from you, it is always nice to know that people like your software and it gives extra motivation. Also if you have idea's to what we could add, please feel free to also let us know.

You can email us at c_m_gitlab@protonmail.com



## License

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.



## FAQ

### What about privacy / is it safe to use?

You don't need to login to twitch and/or grant permissions for the program to run. It just reads the public api from twitch. It has no other communication than with the twitch chat/IRC api and it logges in with the default user that twitch uses for people that aren't logged in.  So in that way, I don't see how it could be any safer. Also everything is computed locally on your pc.

About privacy, if you want to be as possible, download it from gitlab and run it locally. We don't have access information from gitlab, so you should be totally unknown to us. 

For the hosting where you can run it directly, we use an storage provider and as far as we know, we only can see the amount of traffic, but not who. 
However in general if you use an website, you could leak the channelname and your IP. This is how the internet works. Later on, we hope that you can set the channel in the settings, so that you only show your IP. Which you then could mitigate with an VPN.




But to recap, the last mentions are just general remarks. How we have it now, we can only see the amount of visitors, not who.




