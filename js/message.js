/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
let last_message = null
let message = {}
let last_cheer_message = null
 
class ChatMessage{
  constructor(tags, text, self){
    this._ownMessage = self
    this._text = text
    this._tags = tags
    this._channel = settings.channelsByID[this.channelID]
    this._user = this._channel.users.getUserFromMsg(tags)
    this._channel.add_MsgReference(this)

    //tag msg-id: "highlighted-message" == highlight my message
  }

  get channel(){
    return this._channel
  }

  get settings(){
    return this._channel.settings
  }

  get channelID(){
    return this._tags['room-id']
  }

  get user(){
    return this._user
  }

  get id(){
    return this._tags.id
  }

  get type(){
    return this._tags['message-type']
  }

  get divOpen(){
    return `<div class="twi-message ${this.settings.messageClasses}" id="${this.id}" userid="${this.user.id}" roomid="${this.channel.id}">`
  }

  get userHTMLelement(){
    return `<p class='user_name' style='color:${this.user.color}'>
      ${this.user.badgesHTML()}${this.user.name}</p>`
  }

  parseCheermotes(text){
    console.log("check cheer")
    if(!this._tags.bits) return text; // not bits so return

    /* this doesn´t look efficient, but twitch has no clear rules for emote names
    so I guess this is one of the easier ways, thanks Twitch... 
    at least there must be a space for or after emote*/
    log.info("has bits: " + text)

    last_cheer_message = this // only for debug atm


    // lets use same system as emotes for safety, and to keep me sane
    let words = text.split(" ")
    const themeColor = this.settings?.emoteThemeColor || 'dark' // lets assume dark theme as default
    const size = this.settings?.emoteSize || 1;
    for(let i = 0; i < words.length; ++i){
      console.log(words[i])
      if(settings.cheermotes[words[i]]){ // found cheermote, so replace
        console.log('found cheermote ' + words[i])
        const cheermoteholder = '<CM<'+(i)+'>>'
        const url = settings.cheermotes[words[i]].images[themeColor].animated[size]
        words[i] = `<img src="${cheermoteholder}" />`
        this._emoteholders.push({'holder':cheermoteholder, 'url': url})
      }
    }
    return words.join(" ")
  }


  // function to handle / call functions to get the final text result
  get text_parsed(){
    let result = this.parseCheermotes(this.parse_emotes( this._text))
    result = this.parse_atUser(this.parse_formatting(result))
    if(this._tags['message-type'] === 'action'){
      return `<i>${result}</i></p>` }
    else{ 
      return `${this.settings.messageStart}${result}</p>`
    }
  }

// we don´t want to run unknown code, so sanatize the input
//https://remarkablemark.org/blog/2019/11/29/javascript-sanitize-html/
  static sanitizeHTML (text){
    return $('<div>').text(text).html();
  }

  /* regex  from https://stackoverflow.com/a/70200736
     *For bold text*
     _For italic text_
     ~For strikethrough text~
     --For underlined text--
     ```Monospace font``` */
  parse_formatting(s){ 
    let mask = this.settings.permission[this.user.group].mask
    if (settings.isAllowed('bold', mask)){
      s = s.replace(/(?:\*)(?:(?!\s))((?:(?!\*|\n).)+)(?:\*)/g,'<b>$1</b>')
    }
    if (settings.isAllowed('italic', mask)){
      s = s.replace(/(?:_)(?:(?!\s))((?:(?!\n|_).)+)(?:_)/g,'<i>$1</i>')
    }
    if (settings.isAllowed('strikethrough', mask)){
      s = s.replace(/(?:~)(?:(?!\s))((?:(?!\n|~).)+)(?:~)/g,'<s>$1</s>')
    }
    if (settings.isAllowed('underline', mask)){
      s = s.replace(/(?:--)(?:(?!\s))((?:(?!\n|--).)+)(?:--)/g,'<u>$1</u>')
    }
    if (settings.isAllowed('monospace', mask)){
      s = s.replace(/(?:```)(?:(?!\s))((?:(?!\n|```).)+)(?:```)/g,'<tt>$1</tt>');
    }
    for(let i = 0; i < this._emoteholders.length; ++i){
      s = s.replaceAll(this._emoteholders[i].holder, 
        this._emoteholders[i].url)
    }
    return s
  }

// function to parse emotes
// TODO: maybe switch to https://github.com/smilefx/tmi-emote-parse, gives bttv, ffz and 7tv emotes. However cheer emotes will likely still be problem
  parse_emotes(text){
    this._emoteholders = []
    if(this._tags.emotes == null){
      return ChatMessage.sanitizeHTML(text)      
    }
    log.info(this._tags.emotes)
    let l = []
    let dict = {}
    let i = 0
    // get info for each emote
    for( const [key, val] of Object.entries(this._tags.emotes) ){
      //var parts = emotes[i].split(":")
      let url = `https://static-cdn.jtvnw.net/emoticons/v1/${key}/1.0`
      // placeholder is because emotes url´s can have _ (underscore) in them,
      // which messes up de bold formatting, maybe use the emote text als holder, 
      // but this works for now
      let emoteholder = '<EM<'+(i++)+'>>'
      this._emoteholders.push({'holder':emoteholder, 'url': url})
      for(let j = 0; j < val.length; ++j){
        let temp = val[j].split("-")
        l.push(parseInt(temp[0]))
        dict[parseInt(temp[0])] = {'start': parseInt(temp[0]), 'end': parseInt(temp[1]), holder: emoteholder}
      }
    }
    l.sort((a,b) => b-a) // sorts with the numbers, because b-a it will reverse
    var result = ""
    for(  i = 0; i < l.length; ++i){
      result = `<img src="${dict[l[i]].holder}" />${ChatMessage.sanitizeHTML(text.slice(dict[l[i]].end+1))}${result}`
      text = text.slice(0, dict[l[i]].start)
    }
    text= `${ChatMessage.sanitizeHTML(text)}${result}`
    console.log(text)
    return text
  }

// function to color @USER in the color for USER
  parse_atUser (text){
    if(text.indexOf('@') == -1){ // no @ in msg, so return directly
      return text
    }
    let s = text.split('@')
    text = s[0]
    for(var i = 1; i < s.length; ++i){
      var s2 = s[i].split(" ")
      var u = this.channel.users._usersByName[s2.shift() ]
      if(u){ // check if it is user otherwise just print as it was
         text += `<span style='color:${(this.settings.showUserColorWhenMentioned ? u.color : "")}'>
         @${(this.settings.showBadgesWhenMentioned ? u.badgesHTML() : "")}${u.name}
         </span> ${(s2.join(" "))}`
      }
      else{
        text += `@${s[i]}`
      }
    }
    return text
  }

 // function to handle deleted messages through deletion from mod or ban / timeout
  delete(remove_refs = true){
    log.info("Message deleted by mod: "+this.id)
    if(this.settings.removed_message_message){
      $(`#${this.id} .message_text`).text('')
      $(`#${this.id} .message_text`).remove()
      if(!this.settings.removed_message_keep_name){
        $(`#${this.id} .user_name`).remove()
      }
      $('#'+this.id).append(`<p class='message_text deleted_message'>
        ${this.settings.removed_message_message}</p>`)
    }
    else{
      $('#'+this.id).text("")
      this.remove(remove_refs)
    }     
  }

// remove a message, optional remove also the references to the msg on other places
  remove(remove_refs = true){
    log.info(`remove this message ${this.id}`)
    $('#'+this.id).remove();
    if(remove_refs){
      this.channel._removeMsgRef(this)
    }
  }

  doNotShow(){
    // TODO: check if user on ignore list
    // if there is an array, check for all
    if(Array.isArray(this.settings.hideMessagesStartingWith)){
      for(const item of this.settings.hideMessagesStartingWith){
        if(this._text.startsWith(item)) return true
      }
      return false
    }
    // ther is only one, check for that
    return this._text.startsWith(this.settings.hideMessagesStartingWith)
  }


 //  function to create and show the html element for a message
  process(){
    // check if on ignore list
    if(this.doNotShow()){
      log.info("not show msg " + this._text)
      return
    } 

    last_message = this // only used for debug now for easy access in console
    let element = `${this.divOpen}${this.userHTMLelement}<p class='message_text'>
                    ${this.text_parsed}</div>`

    $('#chatwindow').append(element)

    if (this.settings.removeAfter){
      setTimeout(function(){
        log.info("remove: "+this.id);
        $('#'+this.id).remove();}, this.settings.removeAfter*1000)
    }
  }
}

