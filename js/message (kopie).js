/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
var last_message = null
var message = {

  sanitizeHTML: function(text){
    return $('<div>').text(text).html();
  },

  /* regex  from https://stackoverflow.com/a/70200736
     *For bold text*
     _For italic text_
     ~For strikethrough text~
     --For underlined text--
     ```Monospace font``` */
  parse_formatting: function (msg){ 
    let s = msg.parsed
    let mask = msg['channel'].settings.permission[msg['user_role']].mask
    if (settings.isAllowed('bold', mask)){
      s = s.replace(/(?:\*)(?:(?!\s))((?:(?!\*|\n).)+)(?:\*)/g,'<b>$1</b>')
    }
    if (settings.isAllowed('italic', mask)){
      s = s.replace(/(?:_)(?:(?!\s))((?:(?!\n|_).)+)(?:_)/g,'<i>$1</i>')
    }
    if (settings.isAllowed('strikethrough', mask)){
      s = s.replace(/(?:~)(?:(?!\s))((?:(?!\n|~).)+)(?:~)/g,'<s>$1</s>')
    }
    if (settings.isAllowed('underline', mask)){
      s = s.replace(/(?:--)(?:(?!\s))((?:(?!\n|--).)+)(?:--)/g,'<u>$1</u>')
    }
    if (settings.isAllowed('monospace', mask)){
      s = s.replace(/(?:```)(?:(?!\s))((?:(?!\n|```).)+)(?:```)/g,'<tt>$1</tt>');
    }
    for(var i = 0; i < msg.emoteholders.length; ++i){
      s = s.replaceAll(msg.emoteholders[i].holder, 
        msg.emoteholders[i].url)
    }
    msg.parsed = s
    return msg
  },

  // function to create the html element for a message
  format_message: function(msg){
    last_message = msg
    code = `<div class="twi-message ${msg.channel.settings.messageClasses}" id="${msg.id}" userid="${msg['user-id']}" roomid="${msg['room-id']}">
    <p class='user_name' style='color:${msg.user.color}'>${msg.user.badgesHTML()}${msg.user.name}</p><p class='message_text'>`
    //console.log( parse_format(parse_emotes(parse_action(msg))))
   // msg = parse_format(parse_emotes(parse_action(msg)));
    msg = this.parse_formatting( this.parse_emotes( this.parse_action(msg)));
    msg = this.parse_atUser(msg)
    if (msg.action){ msg.parsed = `<i>${msg.parsed}</i>` }
    else{ msg.parsed = `${msg['channel'].settings.messageStart}${msg.parsed}`}
    code += `${msg.parsed}</p></div>`
    
    $('#chatwindow').append(code)

    if (msg['channel'].settings.removeAfter){
      setTimeout(function(){
        msg['twi']._logger("remove: "+msg.id);
        $('#'+msg.id).remove();}, msg['channel'].settings.removeAfter*1000)
    }
  },
  // get the info of the tags twitch send with a message and store them
  proces_tags:function(msg, twi){
    let m = {};
    // for safeguard store twi ws in msg
    m['twi'] = twi
    // get text part
    
    // get tags with the message
    let info = msg.slice(1).split(";")
    for(let i = 0; i < info.length; i++){
      key_val = info[i].split("=");
      m[key_val[0]] = key_val[1] ;
    }
    // check if user is mod or sub :TODOO still need broadcaster
    m['user_role'] = "standard"
    if(parseInt(m['subscriber'])){
      m['user_role'] = "subscriber";
    }
    if(parseInt(m['mod'])){
      m['user_role'] = "moderator";
    }
    //TODO: implement vip
    if( 'room-id' in m){
    // store also channel object in msg for easy use
      m['channel'] = settings.channelsByID[m['room-id']]
    }
    return m
  },

  // get the text part of the message
  proces_text:function(msg, m){
    m['text'] = msg.slice(msg.indexOf(" :")+2).trim()
    return m
  },
  //function to process when twitch sends a message 
  received: function(msg, twi){
    let message = msg['data'];
    message = message.split("PRIVMSG ");
    if(message.length < 2){ // not prvmsg so only info in it
      return this.handleInfoMessage(message[0], twi);
    }

    m = this.proces_tags(message[0], twi)
    m.user = m['channel'].users.getUserFromMsg(m)
    m = this.proces_text(message[1], m)
    if(this.checkNeedToHide(m)){
      // if needed to hide, still can be possible we need to extract info
      // TODO: check ordering, but useless to process many things if we need to not show it
      // but not a priority
      return this.handleInfoMessage(message[0], twi);
    }
    
    this.format_message(m)
  
  },
  // function to parse if it is an /me action message
  parse_action: function(msg){
    if(msg.text.startsWith("\x01ACTION")){
      msg.parsed = msg.text.slice(8,-1) // \x01ACION is 8 char (and last one must be removed)
      msg.action = true
    }
    else{
      msg.parsed = msg.text
      msg.action = false
    }
    return msg
  },
  // function to read and parse the emotes
  parse_emotes: function(msg){
    msg.emoteholders = []
    if(msg.emotes == ""){
      msg.parsed = message.sanitizeHTML(msg.parsed)
      return msg
    }
    log_message(msg.emotes)
    emotes = msg.emotes.split("/")
    l = []
    dict = {}
    
    // get info for each emote
    for(var i = 0; i < emotes.length; i++){
      var parts = emotes[i].split(":")
      var url = `https://static-cdn.jtvnw.net/emoticons/v1/${parts[0]}/1.0`
      var places = parts[1].split(",")
      // placeholder is because emotes url´s can have _ (underscore) in them,
      // which messes up de bold formatting, maybe use the emote text als holder, 
      // but this works for now
      let emoteholder = '<EM<'+i+'>>'
      msg.emoteholders.push({'holder':emoteholder, 'url': url})
      for(var j = 0; j < places.length; ++j){
        let temp = places[j].split("-")
        l.push(parseInt(temp[0]))
        dict[parseInt(temp[0])] = {'start': parseInt(temp[0]), 'end': parseInt(temp[1]), holder: emoteholder}
      }
    }
    l.sort((a,b) => b-a) // sorts with the numbers, because b-a it will reverse
    var result = ""
    for( var i = 0; i < l.length; ++i){
      result = `<img src="${dict[l[i]].holder}" />${message.sanitizeHTML(msg.parsed.slice(dict[l[i]].end+1))}${result}`
      msg.parsed = msg.parsed.slice(0, dict[l[i]].start)
    }
    msg.parsed = `${message.sanitizeHTML(msg.parsed)}${result}`
    console.log(msg)
    return msg
  }
}

// function to give @user in messages the color of the mentioned user
message.parse_atUser = function(msg){
  if(msg.parsed.indexOf('@') == -1){ // no @ in msg, so return directly
    return msg
  }
  let s = msg.parsed.split('@')
  msg.parsed = s[0]
  for(var i = 1; i < s.length; ++i){
    var s2 = s[i].split(" ")
    var u = msg.channel.users._usersByName[s2.shift() ]
    if(u){ // check if it is user otherwise just print as it was
       msg.parsed += `<span style='color:${(msg.channel.settings.showUserColorWhenMentioned ? u.color : "")}'>
       @${(msg.channel.settings.showBadgesWhenMentioned ? u.badgesHTML() : "")}${u.name}
       </span> ${(s2.join(" "))}`
    }
    else{
        msg.parsed += `@${s[i]}`
    }
  }
  return msg
}

message.checkNeedToHide = function(msg){
  for(var i = 0; i < msg.channel.settings.hideMessagesStartingWith.length; ++i){
    if(msg.text.startsWith(msg.channel.settings.hideMessagesStartingWith[i])){
      log_message(`Hide message: message starts with: ${msg.channel.settings.hideMessagesStartingWith[i]}`)
      return true
    }
  }
  return false
}

// function to parse information messages from twitch
message.handleInfoMessage = function (message, twi){
  log_message("Received info message");
  console.log(message);
  // get the emote and badges info of the channel
      if(message.includes("@emote")){ 
        let room_id = parseInt(message.split('room-id=')[1].split(";")[0])
        //console.log("roomdid: "+room_id)
        let ch_name = message.split("ROOMSTATE #")[1].trim()
        try{
            if(settings.channelsByName[ch_name]){
              settings.channelsByName[ch_name].setID_getBadges(room_id) 
            } else{
              let date = new Date()
              log_message("channel not yet initialized, wait 3 sec and try again")
              console.log(`channel not yet initialized, wait 3 sec and try again ${room_id}: ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}` )

              setTimeout(settings.channelsByName[ch_name].setID_getBadges(room_id),3000)
              date = new Date()
              console.log(`after next try ${room_id}: ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`)
            }
         } 
          catch(err){
            console.log(err.message)
            log_message(err.message)
            show_error("Went something wrong with loading, please reload the page") 
          }
      }
      // message removed by mod, so remove from view // change text
      if(message.includes("CLEARMSG")){
        this.clearMSG(message, twi)
      }
      if(message.includes("CLEARCHAT #")){
        this.clearChat(message, twi)
      }
      // if there is ping in message, respond to it
      if( message.includes("PING")){
        twi.send("PONG :tmi.twitch.tv")
        log_message("responded to ping")
      }
      return
}

// function to handle a deleted message 
message.clearMSG = function(msg, twi){
  let m = this.proces_tags(msg, twi)
  m['channel-name'] = msg.split("CLEARMSG #")[1].split(":")[0].trim()
  this.removeMSG(m['target-msg-id'], m['channel-name'], false)
  let s = settings.channelsByName[m['channel-name']].settings
  
  return m
}

// function that will remove all messages *not yet used* 
message.clearChat = function(msg, twi){
  let m = this.proces_tags(msg, twi)
  this.removeBanMSG(m)
}

// function to remove a message (deleted message from mod)
message.removeMSG = function(msg_id, channelName, channelID){
  let s = null
  if(channelName){
    s = settings.channelsByName[channelName].settings
  }
  else if(channelID){
    s = settings.channelsByID[channelID].settings
  }
  else{
    log_message("error for removeMSG, no Channel name or id given / found")
    return
  }
  // del the div with message (set inside to nothing to remove fast as possible if there is an animation)
  if(!(s.removed_message_message)){
    $('#'+msg_id).text("")
    $('#'+msg_id).remove()
    log_message("Message deleted by mod: "+msg_id)
  }
  else{

    $(`#${msg_id} .message_text`).text('')
    $(`#${msg_id} .message_text`).remove()
    if(!s.removed_message_keep_name){
      $(`#${msg_id} .user_name`).remove()
    }
    $('#'+msg_id).append(`<p class='message_text deleted_message'>${s.removed_message_message}</p>`)
    }
}

// function to remove all the messages from banned person
message.removeBanMSG = function(msg){

  // find all messages from userid
  let l = $(`[userid="${msg['target-user-id']}"]`)
  for(var i = 0; i < l.length; ++i){
    //check if same room as user is banned
    let same_room = (parseInt(l[i].attributes["roomid"].value) == parseInt(msg['room-id']))
    if(same_room){
      this.removeMSG(l[i].attributes["id"].value, false, parseInt(msg['room-id']))
    }
  }
}

// simple function to have a log window possible
function log_message(msg){
  let date = new Date()
  let s = `${String(date.getHours()).padStart(2, "0")}:${String(date.getMinutes()).padStart(2, "0")}:${String(date.getSeconds()).padStart(2, "0")} ${msg}`
  $('#console').append(`<p>${s}</p>`)

  console.log(s)
 // $('#chatwindow').append(`<p>${msg}</p>`)
}

