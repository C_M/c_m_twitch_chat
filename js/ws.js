/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
 
class twitch_twi{
  constructor(logger){
    this._client = null
    this.connect()
    this._logger = logger
  }

  get log(){
    return this._logger
  }

  get client(){
    return this.client
  }

  readyStateString(){
    let state = this._client.ws.readyState // maybe find tmi.js function for it
    return `${state} (${this._readyStateStates[state]})`
  }  

  // _onOpen = async () => {
  //   return

  // }

  // _onMessage = (msg) => {
  //   let timeA = performance.now()
  //   message.received(msg, this)
  //   this._logger(`took ${performance.now()-timeA} ms to show message`)
  // }

  connected(){ // same hier, maybe find function for ws
    if(this._client.ws == null || this._client.ws.readyState == undefined){
      return false
    }
    return (this._client.ws.readyState == 1)
  }

  join_channel(name){
    this.send(`JOIN #${name}`)
    new Channel(name)
  }

  leave_channel(name){
    this.send(`LEAVE #${name}`)
  }

  connect(){
    this._client = new tmi.Client({
    options: { debug: true, messagesLogLevel: "info" },
    connection: {
        reconnect: true,
        secure: true
      },
    // identity: {
    //   username: 'bot-name',
    //   password: 'oauth:my-bot-token'
    // },
  //  channels: [ 'my-channel' ]
    });
  this._client.connect().catch(console.error);
  this._client.on('connected', this._onServerConnected.bind(this))
  this._client.on('message', this._onMessage.bind(this) )
  this._client.on('roomstate', this._onJoinedRoom.bind(this))
  this._client.on('join', this._onJoin.bind(this))
  this._client.on('raw_message', this._onRawMessage.bind(this))
  // TODO: still for msg, ban (CLEARCHAT), clearmsg 
  //=> {
//   if(self) return;
//   if(message.toLowerCase() === '!hello') {
//     client.say(channel, `@${tags.username}, heya!`);
//   }
//});

  }

  _onJoinedRoom(channel, tags){
    log.info("joined channel: "+channel)
    console.log(tags)
  }

  _onRawMessage(msg, raw)
  {
    console.log(msg)
    console.log(raw)
  }

  _onMessage(channel, tags, message, self){
    console.log(message)
  }

  _onServerConnected(server, port){
          // TODO: added to channel so get the badges list (or notice that connected etc)
      // dunno if we can use the joined or we need to read rawmessage
    this.log.info(`connected to: ${server}:${port}`)
    settings.onConnected(this)

  }

  _onJoin(channel, nick, self){
    return // we do nothing with it yet
    if(self){ // i'm the one joining
      this.log.info(`joined #${channel} as ${nick}`)

    }
    else{
        this.log.info('someone joined')
      }

  }

  join_channel(channel){
    this._client.join(channel)
    this.log.info(`joining ${channel}`)
  }
  // send(txt){
  //   try{ 
  //     this._socket.send(txt)
  //     this._logger(`Sent: ${txt}`)
  //   }
  //   catch(exception){
  //     this._logger('exception '+ exception);
  //   }
  // }


  disconnect(){
    //needto
  }

  _onClose = () => {
   this.log.info("Socket closed method called " + this.readyStateString());
  }
}
