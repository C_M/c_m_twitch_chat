/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// simple dict to store all the badges information in for the channels
var badges = {
  global: false,
  getGlobalSets: function(){
    return this.global.badge_sets
  }

}

// load the global badges
$.getJSON("https://badges.twitch.tv/v1/badges/global/display?language=en", function(data) {
		badges.global = data;
  });

// // load emotes 
// function loadEmotes(data){
//   settings.emotes = data
// }
// $.getJSON("../resources/cheermotes.json", loadEmotes);

// class that get's created when we add the channel to listen to
// main function for now is to load badges data of a class
class Channel{
  constructor(name){
    this._name = name.toLowerCase()
    this._display_name = name 
    this._ID = null
    this._badges = null
    this.ready = false
    this.users = null
    this.settings = null
    this._tags = null
    this._message_order = []
    this._messages = {}
    settings.channelsByName[this._name] = this
  }

  get maxMsg(){
    return ( this.settings && this.settings.maxMsg || 128)  
  }

  get name(){
    return this._display_name
  }

  get id(){
    return this._ID
  }

  getBadges(){
    return this._badges
  }

  getBadgeSets(){
    return this._badges.badge_sets
  }
  
  getID(){
    return this._ID
  }

  add_MsgReference(msg){
    this._message_order.push(msg.id)
    this._messages[msg.id] = msg
    this.users._users[msg.user.id].messages[msg.id] = msg
    this._checkMaxMsg()
  }

  _checkMaxMsg(){
    while(this._message_order.length > this.maxMsg ){
      log.info('maxMsg hit: remove oldest message')
      let msg = this._messages[this._message_order.shift()]
      msg.remove(false)
      this._removeMsgRefsFromDicts(msg)
    }
  }

  removeMSG(msg_id){
      this._messages[msg_id]?.delete()
  }

  removeMSGofUser(user){
    for ( const [id, msg] of Object.entries(this.users._users[user].messages) ){
      msg.delete()
    }
  }

  _removeMsgRef(msg){
    const index = this._message_order.indexOf(msg.id)
    if(index != -1){ // check if message is in the _message_order array
      this._message_order.splice(index, 1)
    }
    this._removeMsgRefsFromDicts(msg)
  }

  _removeMsgRefsFromDicts(msg){
    delete this._messages[msg.id]
    delete msg.user.messages[msg.id]
  }

// funtion to copy the settings from globalSettings if there are no specific settings
// and gets the badges info for the channel. 
// if badges are load, channel.ready is set to true, this way we can ignore message while
// channel.ready is still false
  setID_getBadges(id){
    this._ID = id
    settings.channelsByID[this._ID] = this;
    if (!(id in settings.byChannel)){
      //maybe logger here
      settings.byChannel[id] = {}
        this.settings = settings.byChannel[this._ID]
        Object.assign(this.settings, settings.globalSettings)
    }
    else{
        this.settings = settings.byChannel[this._ID];
    }
    this.users = new RoomUsers(this._ID)
    const self = this // because of promise / async, copy this to use later
    $.getJSON(`https://badges.twitch.tv/v1/badges/channels/${id}/display?language=en`, function(data) {
      badges[id] = data
      self._badges = badges[id]
      self.ready = true
    });
  }

  set roomState(tags){
    if(!self.ready){ // badges not yet set, so get them
      this.setID_getBadges(tags['room-id'])
    }
    // update the room tags
    this._tags = tags
  }

  getName(){
    return this._name
  }
}

